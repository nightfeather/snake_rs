use std::collections::{ VecDeque, HashMap };
use std::cmp::Ordering;
use std::ops::Add;

extern crate term_size;

#[derive(Copy, Clone)]
enum Direction {
    Up,
    Down,
    Left,
    Right
}

#[derive(Copy, Clone, Eq, Hash)]
struct Coord {
    x: i32,
    y: i32
}

impl Add for Coord {
    type Output = Coord;
    fn add(self, other: Self) -> Self {
        return Self { x: self.x+other.x, y: self.y+other.y };
    }
}

impl PartialEq for Coord {
    fn eq(&self, other: &Self) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

impl PartialOrd for Coord {
    fn partial_cmp(&self, other: &Coord) -> Option<Ordering>{
        return Some(self.cmp(other));
    }
}

impl Ord for Coord {
    fn cmp(&self, other: &Self) -> Ordering{
        return (self.x, self.y).cmp(&(other.x, other.y));
    }
}

struct Snake {
    body: VecDeque<Coord>,
    length: i32,
    direction: Direction,
    alive: bool
}

impl Clone for Snake {
    fn clone(&self) -> Snake {
        let mut vec: VecDeque<Coord> = VecDeque::new();
        for coord in self.body.iter() {
            vec.push_back(*coord);
        }
        return Snake { body: vec, length: self.length, direction: self.direction, alive: self.alive }
    }
}

impl Snake {
    fn stretch(&mut self) {
        let head = self.body.front().unwrap();
        let coord = match self.direction {
            Direction::Up => Coord { x: head.x, y: head.y-1 },
            Direction::Down => Coord { x: head.x, y: head.y+1 },
            Direction::Left => Coord { x: head.x-1, y: head.y },
            Direction::Right => Coord { x: head.x+1, y: head.y }
        };
        self.body.push_front(coord);
    }

    fn forward(&mut self) {
        self.stretch();
        self.body.pop_back();
    }

    fn turn(&mut self, direction: Direction) {
        self.direction = direction;
    }

    fn check(&mut self, stage: &State) {
        let head = self.body.front().unwrap();

        // self intersect
        if self.body.iter().any(|coord| coord == head) {
            self.alive = false;
        }
    }
}

impl Paintable for Snake {
    fn paint(&self, stage: &mut Stage) {
        for coord in self.body.iter() {
            stage.draw_cell(*coord, "⬛".to_string())
        }
    }

}

trait Paintable {
    fn paint(&self, stage: &mut Stage);
}

struct Stage {
    origin: Coord,
    size: Coord,
    buffer: HashMap<Coord, String>,
    stages: Vec<Stage>
}

impl Stage {
    fn draw_cell(&mut self, coord: Coord, content: String) {
        if coord >= self.size { return; }
        self.buffer.insert(coord, content);
    }
}

impl Paintable for Stage {
    fn paint(&self, stage: &mut Stage) {
        for k in self.buffer.keys() {
            let content = self.buffer.get(&k).unwrap().clone();
            stage.buffer.insert(*k+self.origin, content);
        }
    }
}

type Screen = Stage;

struct Renderer {
    screen: Screen,
}

impl Renderer {
    fn render(&self) {
        
    }
}

struct State {
    snake: Snake,
    items: VecDeque<Coord>
}

fn main() {
    if let Some((w,h)) = term_size::dimensions() {
        println!("Size: {}x{}", w, h);
    } else {
        println!("Unable to tell terminal size.")
    }
}
